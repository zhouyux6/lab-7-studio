
words = {}
phrase = """
'hnronxttfao fi x lrnt rl knbxvfdb bphnbiifra. 
hnronxttfao fi x vrrz, tske zfqb xa xnvfiv unsie rn x vd kxtbnx. 
fv kraivnxfai gexv yrs kxa cr knbxvfdbzy, 
usv xzir knbxvbi txay abg rhhrnvsafvf bi lrn favbnxkvfdb bphnbiifra.
"""

for letter in phrase:
    words.setdefault(letter,0)
    words[letter]+=1

for key in sorted(words, key=words.get,reverse=True):
    print(key,":",words[key])

print(words)

thing = "hnronxttfao fi x lrnt rl knbxvfdb bphnbiifra."
print(thing.replace("n", "E"))
thing = "hEroExttfao fi x lrEt rl kEbxvfdb bphEbiifra."
print(thing.replace("r", "O"))
thing = "hEOoExttfao fi x lOEt Ol kEbxvfdb bphEbiifOa."
print(thing.replace("f", "I"))
thing = "hEOoExttIao Ii x lOEt Ol kEbxvIdb bphEbiiIOa."
print(thing.replace("i", "S"))
thing = "hEOoExttIao IS x lOEt Ol kEbxvIdb bphEbSSIOa."
print(thing.replace("x", "A"))
thing = "hEOoEAttIao IS A lOEt Ol kEbAvIdb bphEbSSIOa."
print(thing.replace("a", "N"))
thing = "hEOoEAttINo IS A lOEt Ol kEbAvIdb bphEbSSION."
print(thing.replace("v","F"))
thing = "hEOoEAttINo IS A lOEt Ol kEbAPIdb bphEbSSION."
print(thing.replace("z", "L"))

Key:
{'r': 'O', 'f': 'I', 'i': 'S', 'x': 'A', 'a': 'N', 'v': 'F', 'z': 'L'}